package models.dao;

import java.util.List;

import models.entities.Department;

public interface DepartmentDao {
	
	public Department insert(Department department);
	public Department update(Department department);
	public Department findById(int id);
	public boolean deleteById(int id);
	public List<Department> findAll();
	
}
