package models.dao;

import java.util.List;

import models.entities.Seller;

public interface SellerDao {
	public Seller insert(Seller seller);
	public Seller update(Seller seller);
	public Seller findById(int id);
	public boolean deleteById(int id);
	public List<Seller> findAll();
}
