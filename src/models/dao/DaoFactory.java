package models.dao;

import models.dao.impl.DepartmentDaoJDBC;
import models.dao.impl.SellerDaoJDBC;

public class DaoFactory {

		public static SellerDao createSellerDao() {
			return new SellerDaoJDBC();
		}
		
		public static DepartmentDao createDepartmentDao() {
			return new DepartmentDaoJDBC();
		}
}
