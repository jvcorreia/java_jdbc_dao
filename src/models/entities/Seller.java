package models.entities;

import java.io.Serializable;
import java.util.Date;

public class Seller implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private Date birthDate;
	private double baseSalary;
	private Department department;
	

	public Seller(int id, String name, Date birthDate, double baseSalary, Department department) {
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.baseSalary = baseSalary;
		this.department = department;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public double getBaseSalary() {
		return baseSalary;
	}


	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}


	@Override
	public String toString() {
		return "Seller [id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", baseSalary=" + baseSalary + "]";
	}


	public Department getDepartment() {
		return department;
	}


	public void setDepartment(Department department) {
		this.department = department;
	}
	
	

}
