package models.entities;

import java.io.Serializable;

public class Department implements Serializable {
	
		private static final long serialVersionUID = 1L;
		private int id;
		private String name;
		
		public Department(int id, String name) {
			super();
			this.id = id;
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "Department [id=" + id + ", name=" + name + "]";
		}
		
		
		
}
