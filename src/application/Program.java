package application;

import java.util.Date;

import models.dao.DaoFactory;
import models.dao.SellerDao;
import models.entities.Department;
import models.entities.Seller;

public class Program {

	public static void main(String[] args) {
		
		Department depart1 = new Department(1, "TI");
		
		Seller seller1 = new Seller(1, "Juarez", new Date(), 2500.00, depart1);
		
		SellerDao sellerDao = DaoFactory.createSellerDao();
		
		System.out.println(seller1);
		
	}

}
